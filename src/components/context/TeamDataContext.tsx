import React, { createContext, useState } from "react";

function createData(teamNr: number, teamName: string) {
  return { teamNr, teamName };
}

const data = [
  createData(1, "Team1"),
  createData(0, "Spacer row"),
  createData(2, "Team2"),
  createData(0, "Spacer row"),
  createData(3, "Team3"),
  createData(0, "Spacer row"),
  createData(4, "Team4"),
  createData(0, "Spacer row"),
  createData(5, "Team5"),
];

type Props = {
  children: React.ReactNode;
};

export const TeamDataContext = createContext(data);

export const TeamDataProvider = ({ children }: Props) => {
  const [rows] = useState(data);

  return (
    <TeamDataContext.Provider value={rows}>{children}</TeamDataContext.Provider>
  );
};
